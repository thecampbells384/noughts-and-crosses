import math

import shapes


class Squares:
    def __init__(self, number, x, y, z, c):
        """81 squares are required to fill the three cubes. They can contain 
        X's, O's,and whatever other game items"""
       
        self.number = number
        # The dimension of the faces defined below
        self.box_size = 0.2
        self.x = x
        self.y = y
        self.z = z
        self.c = c
        self.content = shapes.Empty_Box(self.x, self.y, self.z, self.c, self.box_size, "red", self)


class Column:
    def __init__(self, squares, name):
        """A column is one third of a traditional noughts and crosses board
        containing three squares"""
        self.squares = squares
        self.name = name

class Line:
    def __init__(self, square1, square2, square3):
        """A line is any three squares together which, if filled by a player, 
        they win"""
        self.square1 = square1
        self.square2 = square2
        self.square3 = square3
        self.name = f"{square1.number}-{square3.number}"
        self.alternate_name = f"{square3.number}-{square1.number}"


class Layers:
    def __init__(self, columns):
        """A Layer is a 3x3 grid of squares akin to a traditional noughts and
        crosses board. Like a traditional noughts and crosses board, there are 8
        Lines that could result in victory"""
        self.columns = columns
        lines = []
        lines.append(Line(columns[0].squares[0], columns[0].squares[1], columns[0].squares[2]))
        lines.append(Line(columns[1].squares[0], columns[1].squares[1], columns[1].squares[2]))
        lines.append(Line(columns[2].squares[0], columns[2].squares[1], columns[2].squares[2]))
        lines.append(Line(columns[0].squares[0], columns[1].squares[0], columns[2].squares[0]))
        lines.append(Line(columns[0].squares[1], columns[1].squares[1], columns[2].squares[1]))
        lines.append(Line(columns[0].squares[2], columns[1].squares[2], columns[2].squares[2]))
        lines.append(Line(columns[0].squares[0], columns[1].squares[1], columns[2].squares[2]))
        lines.append(Line(columns[0].squares[2], columns[1].squares[1], columns[2].squares[0]))
        self.lines = lines

class Cubes:
    def __init__(self, Layer1, Layer2, Layer3, Layer4, Layer5, Layer6, Layer7,
     Layer8, cube_number):
        """A cube is constructed out of layers 1, 2, and 3 to produce a 3x3x3
        cube. To enable all of the possible lines to be calculated layers 4-8
        are constructed out of the columns along winning lines on the top face
        of the cube"""
        self.layers = [Layer1, Layer2, Layer3, Layer4, Layer5, Layer6,
        Layer7, Layer8]
        self.line_library = {}
        self.cube_number = cube_number
        self.board = shapes.Board(self.cube_number)

    def check_win(self):
        """function which checks the lines in each of the layers of a cube and 
        searches for a winning line. If it finds one it returns win = true, else
        it returns false"""
        win = False
        start_point = []
        end_point = []
        for layer in self.layers:
            # print(f"Layer: {layer}")
            for line in layer.lines:
                # print(f"{line.square1.content} - {line.square2.content} - {line.square3.content}")
                if type(line.square1.content) == type(line.square2.content) == \
                type(line.square3.content) and type(line.square1.content) != shapes.Empty_Box:
                    win = True
                    start_point = [line.square1.x + line.square1.box_size/2, line.square1.y + line.square1.box_size/2, line.square1.z + line.square1.box_size/2]
                    end_point = [line.square3.x + line.square1.box_size/2, line.square3.y + line.square1.box_size/2, line.square3.z + line.square1.box_size/2]
        return win, start_point, end_point


                    
class Cube_set:
    def __init__(self, cubes):
        """A cube set is a set of 3 cubes (cubes 1-3), that, taken together can 
        represent a 3x3x3x3 teseract. Cubes 4-8 are constructed out of layers
        taken from  each of the cubes in the same pattern columns were selected
        to form layers 4-8""" 
        self.cubes = cubes
        self.line_library = {}
        self.players = []

    def reset(self):
        for cube in self.cubes:
            for layer in cube.layers:
                for column in layer.columns:
                    for square in column.squares:
                        square.content = shapes.Empty_Box(square.x, square.y,
                            square.z, square.c, square.box_size, "red", square)

        self.players = []

    def find_player(self):
        """This function identifies and then returns the active player"""
        for player in self.players:
            if player.active:
                active_player = player
        return active_player

    def switch_player(self):
        """This function changes the next player to active and the current player
        to inactive. It returns the new active player"""
        counter = 0
        for player in self.players:
            if player.active:
                player.active = False
                if len(self.players) >= counter + 2: #if the active player is not the last player
                    self.players[counter+1].active = True
                    return self.players[counter+1]
                    break
                else:
                    self.players[0].active = True
                    return self.players[0]
            counter += 1


    def insert_shape(self, player, face):
        """This function changes the content of a given square to the shape of
        a given player"""
        if player.shape.name == "O":
            face.square.content = shapes.O(face.square.x, face.square.y,
                face.square.z, face.square.c, player.colour, face.square.box_size)
        elif player.shape.name == "X":
            face.square.content = shapes.X(face.square.x, face.square.y,
                face.square.z, face.square.c, player.colour, face.square.box_size)
        elif player.shape.name == "Triangle":
            face.square.content = shapes.Triangle(face.square.x, face.square.y,
                face.square.z, face.square.c, player.colour, face.square.box_size)
        elif player.shape.name == "Oct":
            face.square.content = shapes.Oct(face.square.x, face.square.y,
                face.square.z, face.square.c, player.colour, face.square.box_size)

    def create_player_shape(self, shape_number, colour, game_display):
        """This function takes a shape_number and colour from the User_choices
        in the GUI. It creates the shape associated with the shape_number, rotates
        it slightly for asthetic purposes, and returns the produced shape"""
        shape_list = [
            shapes.O(0, -0.13, 0, 0, colour, 0.3, 0.03*2),
            shapes.X(0, -0.13, 0, 0, colour, 0.3, math.sqrt(2*(0.2/5)**2)*1.5, 0.3/5),
            shapes.Triangle(0, -0.13, 0, 0, colour, 0.3),
            shapes.Oct(0, -0.13, 0, 0, colour, 0.3)]

        #The chosen shape is rotated slightly for asthetic purposes

        #if the shape is constructed out of a colection of smaller shapes
        if shape_list[shape_number].subshapes: 
            for shape in shape_list[shape_number].shapes:
                game_display.rotate_shape(shape, 0, 5, 0)
        else: #if the shape is fully described by one instance of Shape
            game_display.rotate_shape(shape_list[shape_number], 0, 5, 0)

        return shape_list[shape_number]




    def create_players(self, User_choices, game_space, game_display):
        """This function takes the user selected player attributes from the GUI
        and uses it to create a Player class for each player, stored within the
        cube set, that contains the players colour and shape. It then turns player
        1 to active and displays their shape and name to the player_display within
        game_display"""
        
        # The count is used to give the appropriate player.name
        player_count = 1
        #for each set of player data an instance of player is created
        for (colour, shape_number, computer) in zip(User_choices.colours, User_choices.shapes, User_choices.computer):
            if computer == "human":
                computer = False
            else:
                computer == True
            player = Player(f"player {player_count}",
                self.create_player_shape(shape_number.get(), colour.get(), game_display),
                colour.get(), computer)
            game_space.players.append(player)
            player_count += 1

        #player 1 is set to active and their attributes displayed on the player_display
        game_space.players[0].active = True
        game_display.display_player(game_space.players[0], game_space.players[0].shape)


class Player:
    def __init__(self, name, shape, colour, computer):
        self.colour = colour
        self.shape = shape
        self.active = False
        self.name = name
        self.computer = computer
        
 
def create_squares():
    # creates 81 squares 
    squares = []
    x_coords=[]
    y_coords=[]
    z_coords=[]
    c_coords=[]
    x_coord = -1.5
    y_coord = -1.5
    z_coord = -1.5
    c_coord = 0
    # A loop to give x, y, z, and c coordinates to the squares in the correct order
    # for how they are placed in the cubes. Coordinates chosen to make the centre
    # of each cube 0,0,0.
    for number in range(81):
        x_coords.append(x_coord)
        y_coords.append(y_coord)
        z_coords.append(z_coord)
        c_coords.append(c_coord)
        y_coord = y_coord + 1
        if y_coord == 1.5:
            y_coord = -1.5
            z_coord = z_coord + 1
        if z_coord == 1.5:
            z_coord = -1.5
            x_coord = x_coord + 1
        if x_coord == 1.5:
            x_coord = -1.5
            c_coord = c_coord + 1
    # for number in range(18):
        # print(x_coords[number], y_coords[number], z_coords[number], c_coords[number])


    for number in range(81):
        squares.append(Squares(number, x_coords[number], y_coords[number], z_coords[number], c_coords[number]))

  
    return squares

def create_columns(squares):
    # creates 27 columns and fills them sequentially with 3 squares
    columns = []
    square_number = 0
    for number in range(27):
        columns.append(Column(squares[square_number: square_number + 3], number))
        square_number = square_number + 3


    return columns

def create_layers(columns):
    column_number = 0
    layers = []

    # creates 9 layers (each constructed out of 3 columns) sequentially using all 27
    # columns. These layers resemble traditional noughts and crosses boards. Layers
    # 1-3 will represent the first cube, 4-6 the second, and 3-9 the third. 
    for number in range(9):
        layers.append(Layers(columns[column_number: column_number + 3]))
        column_number = column_number + 3



    special_layers = []

    special_layers1 = [] # Layer representing top horizontal line on the upper face of the cube
    special_layers2 = [] # Layer representing  diagonal line from top left to bottom right on the upper face of the cube
    special_layers3 = [] # Layer representing middle horizontal line on the upper face of the cube
    special_layers4 = [] # Layer representing bottom horizontal line on the upper face of the cube
    special_layers5 = [] # Layer representing  diagonal line from bottom left to top right on the upper face of the cube

    # This is the construction of the layers necessary for discovering lines which
    # intersect the cube, for cubes 1, 2, and 3.
    column_number = 0
    for number in range(3):
        special_layers1.append(Layers([columns[column_number],
         columns[column_number + 3], columns[column_number + 6]]))
        special_layers2.append(Layers([columns[column_number],
         columns[column_number + 4], columns[column_number + 8]]))
        special_layers3.append(Layers([columns[column_number + 1],
         columns[column_number + 4], columns[column_number + 7]]))
        special_layers4.append(Layers([columns[column_number + 2],
         columns[column_number + 5], columns[column_number + 8]]))
        special_layers5.append(Layers([columns[column_number + 2],
         columns[column_number + 4], columns[column_number + 6]]))
        column_number = column_number + 9 # adding 9 columns itterates to the next cube




    # These are the equivalent layers for cubes 4-8. These were done manually as
    # the irregular column numbers made finding an appropriate loop chalanging.
    special_layers1.append(Layers([columns[0], columns[9], columns[18]]))
    special_layers2.append(Layers([columns[0], columns[10], columns[20]]))
    special_layers3.append(Layers([columns[1], columns[10], columns[19]]))
    special_layers4.append(Layers([columns[2], columns[11], columns[20]]))
    special_layers5.append(Layers([columns[2], columns[10], columns[18]]))

    special_layers1.append(Layers([columns[0], columns[12], columns[24]]))
    special_layers2.append(Layers([columns[0], columns[13], columns[26]]))
    special_layers3.append(Layers([columns[1], columns[13], columns[25]]))
    special_layers4.append(Layers([columns[2], columns[14], columns[26]]))
    special_layers5.append(Layers([columns[2], columns[13], columns[24]]))

    special_layers1.append(Layers([columns[3], columns[12], columns[21]]))
    special_layers2.append(Layers([columns[3], columns[13], columns[23]]))
    special_layers3.append(Layers([columns[4], columns[13], columns[22]]))
    special_layers4.append(Layers([columns[5], columns[14], columns[23]]))
    special_layers5.append(Layers([columns[5], columns[13], columns[21]]))

    special_layers1.append(Layers([columns[6], columns[15], columns[24]]))
    special_layers2.append(Layers([columns[6], columns[16], columns[26]]))
    special_layers3.append(Layers([columns[7], columns[16], columns[25]]))
    special_layers4.append(Layers([columns[8], columns[17], columns[26]]))
    special_layers5.append(Layers([columns[8], columns[16], columns[24]]))

    special_layers1.append(Layers([columns[6], columns[12], columns[18]]))
    special_layers2.append(Layers([columns[6], columns[13], columns[20]]))
    special_layers3.append(Layers([columns[7], columns[13], columns[19]]))
    special_layers4.append(Layers([columns[8], columns[14], columns[20]]))
    special_layers5.append(Layers([columns[8], columns[13], columns[18]]))

    special_layers.append(special_layers1)
    special_layers.append(special_layers2)
    special_layers.append(special_layers3)
    special_layers.append(special_layers4)
    special_layers.append(special_layers5)



    return layers, special_layers


def create_cubes(layers, special_layers):

    layer_number = 0
    cubes = []
    # This creates cubes 1-3
    for number in range(3):
        cubes.append(Cubes(layers[layer_number], layers[layer_number + 1],
            layers[layer_number + 2], special_layers[0][number], special_layers[1][number],
            special_layers[2][number], special_layers[3][number], special_layers[4][number], number))
        layer_number = layer_number + 3

    #These are the cubes constructed of layers from each of the 3 "real" cubes so
    # that the 4 dimensional lines can be calculated
    cubes.append(Cubes(layers[0], layers[3], layers[6], special_layers[0][3],
        special_layers[1][3], special_layers[2][3], special_layers[3][3],
        special_layers[4][3], 3))
    cubes.append(Cubes(layers[0], layers[4], layers[8], special_layers[0][4],
        special_layers[1][4], special_layers[2][4], special_layers[3][4],
        special_layers[4][4], 4))
    cubes.append(Cubes(layers[1], layers[4], layers[7], special_layers[0][5],
        special_layers[1][5], special_layers[2][5], special_layers[3][5],
        special_layers[4][5],5))
    cubes.append(Cubes(layers[2], layers[5], layers[8], special_layers[0][6],
        special_layers[1][6], special_layers[2][6], special_layers[3][6],
        special_layers[4][6], 6))
    cubes.append(Cubes(layers[2], layers[4], layers[6], special_layers[0][7],
        special_layers[1][7], special_layers[2][7], special_layers[3][7],
        special_layers[4][7], 7))



    return cubes




def create_board():
    squares = create_squares()
    columns = create_columns(squares)
    layers, special_layers = create_layers(columns)
    cubes = create_cubes(layers, special_layers)
    game_space = Cube_set(cubes)
    return game_space


game_space = create_board()


# for cube in game_space.cubes:
# for layer in game_space.cubes[0].layers[0:3]:
#     for column in layer.columns:
#         print(f"'{column}',")

# print()
# # for cube in game_space.cubes:
# for layer in game_space.cubes[0].layers[3:8]:
#     for column in layer.columns:
#         print(f"'{column}',")

# print(game_space.cubes[0].layers[0].columns[0])

# for layer in game_space.cubes[0].layers[3:8]:
#     for column in layer.columns:
#         print(column)



# game_space.cubes[0].layers[0].columns[0].squares[0].content = "x"
# game_space.cubes[0].layers[0].columns[1].squares[0].content = "x"
# game_space.cubes[0].layers[0].columns[2].squares[0].content = "x"
# print(game_space.cubes[0].check_win())

# line_library = []
# for cube in game_space.cubes:
#     for layer in cube.layers:
#         for line in layer.lines:
#             if line.name not in line_library:
#                 line_library.append(line.name)

# print (line_library)
# print (len(line_library))