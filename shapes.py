import math
import tkinter
from statistics import mean


from game_display import Coordinates
import gui


class Face:
    def __init__(self, points, c, colour, shape, square = None, Projected_Points=[]):
        self.points = points
        self.Projected_Points = Projected_Points
        list_z = []
        list_x = []
        list_y = []
        for point in self.points:
            list_z.append(point.z), list_y.append(point.y), list_x.append(point.x)
        self.z = mean(list_z)
        self.y = mean(list_y)
        self.x = mean(list_x)
        self.c = c
        #vector from the face's parent shape to the face's centre point
        self.orientation_vector = []
        self.distance = []
        self.colour = colour
        self.shape = shape
        self.physical_face = []
        self.square = square
        self.window = []
        self.game_space = []
        self.gui = []
        self.AI = []

    def clicked(self, event):
        """This function is ran when a player clicks on an empty box. It identifies
        the player, replaces the contents of the square with their shape, rotates
        the coordinates of that shape by the cumulative x, y, and z rotations
        experienced in the game so far, updates the shape list within window and
        redisplays the shapes. Finally it checks for a winner before switching player"""

        
        # Only empty boxes should be replacable by players shapes 
        if type(self.shape) == Empty_Box:
            # Identifies the active player
            player = self.game_space.find_player()
            #replaces the empty square with the players shape
            self.game_space.insert_shape(player, self)
            
            #adjusts the coordinates and orientation of the new shape for any 
            #rotations of the game board that have occured during playing
            if self.square.content.subshapes:
                self.window.rotate_points(self.square.content.shapes,
                    self.window.cumulative_x_rotate, self.window.cumulative_y_rotate,
                    self.window.cumulative_z_rotate)
            else:
                self.window.rotate_points(self.square.content,
                    self.window.cumulative_x_rotate, self.window.cumulative_y_rotate,
                    self.window.cumulative_z_rotate)

            # refreshes the shape list within window and then displays the shapes
            self.window.obtain_shapes(self.window, self.game_space)
            self.window.display_shapes(self.window.shapes)

            # Checks for a winner
            for cube in self.game_space.cubes:
                win, start_point, end_point = cube.check_win()
                if win:
                    self.display_win(player)
                    break
            if not win:
                # Switches Player
                player = self.game_space.switch_player()
                self.window.display_player(player, player.shape)
                print(f"{player.name} is computer: {player.computer.get()}")
                if player.computer.get() == "computer":
                    self.AI.computer_move()

    def display_win(self, player):
        # start_x, start_y, start_z = start_point
        # terminal_x, terminal_y, terminal_z = end_point
        # num_increments = 100
        # colour = "black"

        # x_increment = (terminal_x - start_x)/num_increments
        # y_increment = (terminal_y - start_y)/num_increments
        # z_increment = (terminal_z - start_z)/num_increments

        # lines = []
        # end_x = start_x + x_increment
        # end_y = start_y + y_increment
        # end_z = start_z + z_increment

        # for increment in range(num_increments):
        #     lines.append(Line(Coordinates([start_x, start_y, start_z]),Coordinates([end_x, end_y, end_z]), colour))
        #     start_x = end_x
        #     start_y = end_y
        #     start_z = end_z
        #     end_x += x_increment
        #     end_y += y_increment
        #     end_z += z_increment

        # for line in lines:
        #     self.window.shapes.append(line)
        # self.window.display_shapes(self.window.shapes)

        self.window.player_display.config(highlightthickness=15, highlightbackground="red")
        self.window.player_label.config(text=f"{player.name} wins!!!")

        self.window.button_frame.destroy()
        quit_button = tkinter.Button(self.window.frame, text="Quit", font=(18),
            width=30, height=2, command=lambda:self.end_game())
        quit_button.pack(side="left")
        play_again_button = tkinter.Button(self.window.frame, text="play again",
            font=(18), width=30, height=2, command=lambda:self.play_again())
        play_again_button.pack()

    def end_game(self):
        self.gui.quit()

    def play_again(self):
        self.game_space.reset()
        self.gui.switch_frame(gui.StartPage, None)


class Line:
    def __init__(self, start_point, end_point, colour):
        self.colour = colour
        self.start_point = start_point
        self.end_point = end_point
        self.line = True
        self.x = (end_point.x - start_point.x)/2
        self.y = (end_point.y - start_point.y)/2
        self.z = (end_point.z - start_point.z)/2 

class Shape:
    def __init__(self, faces, square = None):
        self.faces = faces
        self.subshapes = False
        self.square = square
        self.line = False
        list_z = []
        list_y = []
        list_x = []
        for face in faces:
            list_z.append(face.z)
            list_y.append(face.y)
            list_x.append(face.x)
        self.z = mean(list_z)
        self.y = mean(list_y)
        self.x = mean(list_x)

class Board:
    def __init__(self, c_coord, board_thickness = 0.02, box_size = 0.02,
        horizontal_offset = 0.5, vertical_offset = 0.5):
        
        self.board_thickness = board_thickness
        self.box_size = box_size
        self.horizontal_offset = horizontal_offset
        self.vertical_offset = vertical_offset
        self.shapes = self.create_board(c_coord)



    def create_board(self, c_coord):
        """A function which loops through all the coordinates for squares in a cube.
        It constructs walls sandwitching the middle row and column of each layer in 
        the cube, resulting in three traditional noughts and crosses boards which
        make it easier to visualise where the shapes are in space"""
        shapes = []
        x_coord = -1.5
        y_coord = -1.5
        z_coord = -1.5
        for number in range(27):
            if z_coord == -0.5:
                shapes.append(Shape(self.create_vertical_walls(x_coord, y_coord, z_coord, c_coord,
                    self.box_size, self.horizontal_offset, self.vertical_offset, self.board_thickness)))
                shapes.append(Shape(self.create_vertical_walls(x_coord, y_coord, z_coord + 1, c_coord,
                    self.box_size, self.horizontal_offset, self.vertical_offset, self.board_thickness)))

            if y_coord == -0.5:
                shapes.append(Shape(self.create_horizontal_walls(x_coord, y_coord, z_coord, c_coord,
                    self.box_size, self.horizontal_offset, self.vertical_offset, self.board_thickness)))
                shapes.append(Shape(self.create_horizontal_walls(x_coord, y_coord + 1, z_coord, c_coord,
                    self.box_size, self.horizontal_offset, self.vertical_offset, self.board_thickness)))

            y_coord = y_coord + 1
            if y_coord == 1.5:
                y_coord = -1.5
                z_coord = z_coord + 1
            if z_coord == 1.5:
                z_coord = -1.5
                x_coord = x_coord + 1

        return shapes

 
    def create_vertical_walls(self, x_coord, y_coord, z_coord, c_coord, box_size,
            horizontal_offset, vertical_offset, board_thickness):
        shape = []
        shape.append(Face([Coordinates([x_coord, y_coord - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord + box_size, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord - horizontal_offset])], c_coord, "grey", self))
        shape.append(Face([Coordinates([x_coord, y_coord + vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord, y_coord - vertical_offset, z_coord - horizontal_offset - board_thickness]),
            Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord - horizontal_offset - board_thickness]),
            Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord - horizontal_offset])], c_coord, "grey", self))
        shape.append(Face([Coordinates([x_coord, y_coord - vertical_offset, z_coord - horizontal_offset - board_thickness]),
            Coordinates([x_coord, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset - board_thickness]),
            Coordinates([x_coord + box_size, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset - board_thickness]),
            Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord - horizontal_offset - board_thickness])], c_coord, "grey", self))
        shape.append(Face([Coordinates([x_coord, y_coord - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord, y_coord - vertical_offset, z_coord - horizontal_offset - board_thickness]),
            Coordinates([x_coord, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset - board_thickness]),
            Coordinates([x_coord, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset])], c_coord, "grey", self))
        shape.append(Face([Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord - horizontal_offset - board_thickness]),
            Coordinates([x_coord + box_size, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset - board_thickness]),
            Coordinates([x_coord + box_size, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset])], c_coord, "grey", self))
        shape.append(Face([Coordinates([x_coord, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset - board_thickness]),
            Coordinates([x_coord + box_size, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset - board_thickness]),
            Coordinates([x_coord + box_size, y_coord + (1-board_thickness) - vertical_offset, z_coord - horizontal_offset])], c_coord, "grey", self))
        return shape

    def create_horizontal_walls(self, x_coord, y_coord, z_coord, c_coord, box_size,
            horizontal_offset, vertical_offset, board_thickness):
        shape = []
        shape.append(Face([Coordinates([x_coord, y_coord - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord, y_coord - vertical_offset, z_coord + 1 - horizontal_offset]),
            Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord + 1 - horizontal_offset]),
            Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord - horizontal_offset])], c_coord, "grey", self))
        shape.append(Face([Coordinates([x_coord, y_coord - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord, y_coord - board_thickness - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord + box_size, y_coord - board_thickness - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord - horizontal_offset])], c_coord, "grey", self))
        shape.append(Face([Coordinates([x_coord, y_coord - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord, y_coord - board_thickness - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord, y_coord - board_thickness - vertical_offset, z_coord - horizontal_offset + 1]),
            Coordinates([x_coord, y_coord - vertical_offset, z_coord - horizontal_offset + 1])], c_coord, "grey", self))
        shape.append(Face([Coordinates([x_coord, y_coord - board_thickness - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord, y_coord - board_thickness - vertical_offset, z_coord + 1 - horizontal_offset]),
            Coordinates([x_coord + box_size, y_coord - board_thickness - vertical_offset, z_coord + 1 - horizontal_offset]),
            Coordinates([x_coord + box_size, y_coord - board_thickness - vertical_offset, z_coord - horizontal_offset])], c_coord, "grey", self))
        shape.append(Face([Coordinates([x_coord, y_coord - vertical_offset, z_coord - horizontal_offset + 1]),
            Coordinates([x_coord, y_coord - board_thickness - vertical_offset, z_coord - horizontal_offset + 1]),
            Coordinates([x_coord + box_size, y_coord - board_thickness - vertical_offset, z_coord - horizontal_offset + 1]),
            Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord - horizontal_offset + 1])], c_coord, "grey", self))
        shape.append(Face([Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord + box_size, y_coord - board_thickness - vertical_offset, z_coord - horizontal_offset]),
            Coordinates([x_coord + box_size, y_coord - board_thickness - vertical_offset, z_coord - horizontal_offset + 1]),
            Coordinates([x_coord + box_size, y_coord - vertical_offset, z_coord - horizontal_offset + 1])], c_coord, "grey", self))

        return shape


class X:
    def __init__(self, x_coord, y_coord, z_coord, c_coord, colour, box_size,
        thickness = math.sqrt(2*(0.2/5)**2), offset = 0.2/5):
        self.subshapes = True
        self.thickness = thickness
        self.shapes = []
        self.shapes.append(self.create_arms(x_coord, y_coord, z_coord, c_coord,
            box_size, offset, thickness, box_size, offset, colour))
        self.shapes.append(self.create_arms(x_coord + box_size/2,
            y_coord + box_size/2, z_coord, c_coord, box_size, offset, thickness,
            box_size, offset, colour))
        self.shapes.append(self.create_arms(x_coord, y_coord + box_size, z_coord,
            c_coord, box_size, offset, self.thickness, -box_size, -offset, colour))
        self.shapes.append(self.create_arms(x_coord + box_size/2,
            y_coord + box_size/2, z_coord, c_coord, box_size, offset, self.thickness,
            -box_size, -offset, colour))
        self.shapes.append(self.create_centre(x_coord, y_coord, z_coord, c_coord,
            box_size, offset, self.thickness, box_size, offset, colour))
        self.colour = colour
        self.name = "X"
        
    def create_centre(self, x_coord, y_coord, z_coord, c_coord, box_size, offset,
        thickness, y_box_size, y_offset, colour):
        faces = []
        faces.append(Face([Coordinates([x_coord + box_size/2, y_coord + y_box_size/2 - y_offset, z_coord]),
            Coordinates([x_coord + box_size/2 + offset, y_coord + y_box_size/2, z_coord]),
            Coordinates([x_coord + box_size/2, y_coord + y_box_size/2 + offset, z_coord]),
            Coordinates([x_coord + box_size/2 - offset, y_coord + y_box_size/2, z_coord])], c_coord, colour, self))
        faces.append(Face([Coordinates([x_coord + box_size/2, y_coord + y_box_size/2 - y_offset, z_coord + thickness]),
            Coordinates([x_coord + box_size/2 + offset, y_coord + y_box_size/2, z_coord + thickness]),
            Coordinates([x_coord + box_size/2, y_coord + y_box_size/2 + offset, z_coord + thickness]),
            Coordinates([x_coord + box_size/2 - offset, y_coord + y_box_size/2, z_coord + thickness])], c_coord, colour, self))
        shape = Shape(faces)
        return shape

    def create_arms(self, x_coord, y_coord, z_coord, c_coord, box_size, offset,
        thickness, y_box_size, y_offset, colour):
        faces = []
        faces.append(Face([Coordinates([x_coord + offset, y_coord, z_coord]),
            Coordinates([x_coord + box_size/2, y_coord + y_box_size/2 - y_offset, z_coord]),
            Coordinates([x_coord + box_size/2 - offset, y_coord + y_box_size/2, z_coord]),
            Coordinates([x_coord, y_coord + y_offset, z_coord])], c_coord, colour, self))
        faces.append(Face([Coordinates([x_coord + offset, y_coord, z_coord]),
            Coordinates([x_coord + offset, y_coord, z_coord + thickness]),
            Coordinates([x_coord, y_coord + y_offset, z_coord + thickness]),
            Coordinates([x_coord, y_coord + y_offset, z_coord])], c_coord, colour, self))
        faces.append(Face([Coordinates([x_coord + offset, y_coord, z_coord]),
            Coordinates([x_coord + offset, y_coord, z_coord + thickness]),
            Coordinates([x_coord + box_size/2, y_coord + y_box_size/2 - y_offset, z_coord + thickness]),
            Coordinates([x_coord + box_size/2, y_coord + y_box_size/2 - y_offset, z_coord])], c_coord, colour, self))
        faces.append(Face([Coordinates([x_coord + offset, y_coord, z_coord + thickness]),
            Coordinates([x_coord + box_size/2, y_coord + y_box_size/2 - y_offset, z_coord + thickness]),
            Coordinates([x_coord + box_size/2 - offset, y_coord + y_box_size/2, z_coord + thickness]),
            Coordinates([x_coord, y_coord + y_offset, z_coord + thickness])], c_coord, colour, self))
        faces.append(Face([Coordinates([x_coord + box_size/2, y_coord + y_box_size/2 - y_offset, z_coord]),
            Coordinates([x_coord + box_size/2, y_coord + y_box_size/2 - y_offset, z_coord + thickness]),
            Coordinates([x_coord + box_size/2 - offset, y_coord + y_box_size/2, z_coord + thickness]),
            Coordinates([x_coord + box_size/2 - offset, y_coord + y_box_size/2, z_coord])], c_coord, colour, self))
        faces.append(Face([Coordinates([x_coord, y_coord + y_offset, z_coord]),
            Coordinates([x_coord, y_coord + y_offset, z_coord + thickness]),
            Coordinates([x_coord + box_size/2 - offset, y_coord + y_box_size/2, z_coord + thickness]),
            Coordinates([x_coord + box_size/2 - offset, y_coord + y_box_size/2, z_coord])], c_coord, colour, self))
        subshape = Shape(faces)
        return subshape

class O:
    def __init__(self, x_coord, y_coord, z_coord, c_coord, colour, box_size,
        thickness = 0.03, width = math.sqrt(2*(0.2/5)**2)):
        """A class for creating O's. The O is constructed out of a series of small trapazoids"""
        # Distance from the centre of the O to the inner corner of a trapazoid
        self.centre_to_vertex = box_size/2
        # The O shape is constructed out of a series of subshapes
        self.subshapes = True
        self.thickness = thickness
        #coordinates for constructing the trapaziods
        self.shape_points = self.get_points(x_coord,y_coord,z_coord,box_size,self.thickness,width,self.centre_to_vertex)
        self.shapes = self.create_trapazoids(colour, self.shape_points, c_coord)

        self.name = "O"


    def get_points(self, x_coord, y_coord, z_coord, box_size, thickness, width, centre_to_vertex):
        """This function determines the coordinates for each point on all of the trapazoids"""
        points = []
        shape_points = []
        #The fraction, when multiplied by pi and divided by 100, produces a series of angles
        #between 0 and 2pi. The outer corners of the front face of a trapazoid are found
        #at a distance centre_to_vertex from the centre of the O at angles plus and minus
        #pi/24 of angle. The inner points are defined at a distance centre_to_vertex minus thickness

        for fraction in range(0,225,8):
            angle = fraction*math.pi/100
            #Front four trapazoid corners
            points.append(Coordinates([centre_to_vertex*math.cos(angle + math.pi/24),
                centre_to_vertex*math.sin(angle + math.pi/24), z_coord]))
            points.append(Coordinates([centre_to_vertex*math.cos(angle - math.pi/24),
                centre_to_vertex*math.sin(angle - math.pi/24), z_coord]))
            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle - math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle - math.pi/24), z_coord]))
            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle + math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle + math.pi/24), z_coord]))

            #Back four trapazoid corners
            points.append(Coordinates([centre_to_vertex*math.cos(angle + math.pi/24),
                centre_to_vertex*math.sin(angle + math.pi/24), z_coord + width]))
            points.append(Coordinates([centre_to_vertex*math.cos(angle - math.pi/24),
                centre_to_vertex*math.sin(angle - math.pi/24), z_coord + width]))
            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle - math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle - math.pi/24), z_coord + width]))
            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle + math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle + math.pi/24), z_coord + width]))

            points.append(Coordinates([centre_to_vertex*math.cos(angle + math.pi/24),
                centre_to_vertex*math.sin(angle + math.pi/24), z_coord]))
            points.append(Coordinates([centre_to_vertex*math.cos(angle - math.pi/24),
                centre_to_vertex*math.sin(angle - math.pi/24), z_coord]))
            points.append(Coordinates([centre_to_vertex*math.cos(angle - math.pi/24),
                centre_to_vertex*math.sin(angle - math.pi/24), z_coord + width]))
            points.append(Coordinates([centre_to_vertex*math.cos(angle + math.pi/24),
                centre_to_vertex*math.sin(angle + math.pi/24), z_coord + width]))

            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle - math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle - math.pi/24), z_coord]))
            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle + math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle + math.pi/24), z_coord]))
            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle + math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle + math.pi/24), z_coord + width]))
            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle - math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle - math.pi/24), z_coord + width]))

            points.append(Coordinates([centre_to_vertex*math.cos(angle - math.pi/24),
                centre_to_vertex*math.sin(angle - math.pi/24), z_coord]))
            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle - math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle - math.pi/24), z_coord]))
            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle - math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle - math.pi/24), z_coord + width]))
            points.append(Coordinates([centre_to_vertex*math.cos(angle - math.pi/24),
                centre_to_vertex*math.sin(angle - math.pi/24), z_coord + width]))

            points.append(Coordinates([centre_to_vertex*math.cos(angle + math.pi/24),
                centre_to_vertex*math.sin(angle + math.pi/24), z_coord]))
            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle + math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle + math.pi/24), z_coord]))            
            points.append(Coordinates([(centre_to_vertex - thickness)*math.cos(angle + math.pi/24),
                (centre_to_vertex - thickness)*math.sin(angle + math.pi/24), z_coord + width]))
            points.append(Coordinates([centre_to_vertex*math.cos(angle + math.pi/24),
                centre_to_vertex*math.sin(angle + math.pi/24), z_coord + width]))            


            # This loop moves the orign from the centre of the O to the bottom
            #left hand corner

           
            for point in points:
                point.x += x_coord + box_size/2
                point.y += y_coord + box_size/2
       

            shape_points.append(points)
            points = []
        return shape_points


    def create_trapazoids(self, colour, shape_points, c_coord):
        """Given the coorect coordinates this function produces trapazoids in the 
        shape class"""
        faces = []
        shapes = []
        for shape in shape_points:
            for point in range(0,24,4):
                faces.append(Face([shape[point], shape[point+1], shape[point+2],
                    shape[point+3]], c_coord, colour, self))

            shapes.append(Shape(faces))
            faces = []
        return shapes

class Oct:
    def __init__(self, x_coord, y_coord, z_coord, c_coord, colour, box_size,
        width = math.sqrt(2*(0.2/5)**2)):
        """A class for creating Octagons"""
        self.subshapes = False
        #coordinates for constructing the Octagon
        self.points = self.get_points(x_coord,y_coord,z_coord,box_size,width)
        self.faces = self.get_faces(self.points, colour, c_coord)
        self.line = False
        self.name = "Oct"


    def get_points(self, x_coord, y_coord, z_coord, box_size, width):
        points = []
        points.append(Coordinates([x_coord + box_size/3, y_coord, z_coord]))
        points.append(Coordinates([x_coord + 2*box_size/3, y_coord, z_coord]))
        points.append(Coordinates([x_coord + box_size, y_coord + box_size/3, z_coord]))
        points.append(Coordinates([x_coord + box_size, y_coord + 2*box_size/3, z_coord]))
        points.append(Coordinates([x_coord + 2*box_size/3, y_coord + box_size, z_coord]))
        points.append(Coordinates([x_coord + box_size/3, y_coord + box_size, z_coord]))
        points.append(Coordinates([x_coord, y_coord + 2*box_size/3, z_coord]))
        points.append(Coordinates([x_coord, y_coord + box_size/3, z_coord]))

        points.append(Coordinates([x_coord + box_size/3, y_coord, z_coord + width]))
        points.append(Coordinates([x_coord + 2*box_size/3, y_coord, z_coord + width]))
        points.append(Coordinates([x_coord + box_size, y_coord + box_size/3, z_coord + width]))
        points.append(Coordinates([x_coord + box_size, y_coord + 2*box_size/3, z_coord + width]))
        points.append(Coordinates([x_coord + 2*box_size/3, y_coord + box_size, z_coord + width]))
        points.append(Coordinates([x_coord + box_size/3, y_coord + box_size, z_coord + width]))
        points.append(Coordinates([x_coord, y_coord + 2*box_size/3, z_coord + width]))
        points.append(Coordinates([x_coord, y_coord + box_size/3, z_coord + width]))

        points.append(Coordinates([x_coord + box_size/3, y_coord, z_coord]))
        points.append(Coordinates([x_coord + 2*box_size/3, y_coord, z_coord]))
        points.append(Coordinates([x_coord + 2*box_size/3, y_coord, z_coord + width]))
        points.append(Coordinates([x_coord + box_size/3, y_coord, z_coord + width]))

        points.append(Coordinates([x_coord + 2*box_size/3, y_coord, z_coord]))
        points.append(Coordinates([x_coord + box_size, y_coord + box_size/3, z_coord]))
        points.append(Coordinates([x_coord + box_size, y_coord + box_size/3, z_coord + width]))
        points.append(Coordinates([x_coord + 2*box_size/3, y_coord, z_coord + width]))

        points.append(Coordinates([x_coord + box_size, y_coord + box_size/3, z_coord]))
        points.append(Coordinates([x_coord + box_size, y_coord + 2*box_size/3, z_coord]))
        points.append(Coordinates([x_coord + box_size, y_coord + 2*box_size/3, z_coord + width]))
        points.append(Coordinates([x_coord + box_size, y_coord + box_size/3, z_coord + width]))

        points.append(Coordinates([x_coord + box_size, y_coord + 2*box_size/3, z_coord]))
        points.append(Coordinates([x_coord + 2*box_size/3, y_coord + box_size, z_coord]))
        points.append(Coordinates([x_coord + 2*box_size/3, y_coord + box_size, z_coord + width]))
        points.append(Coordinates([x_coord + box_size, y_coord + 2*box_size/3, z_coord + width]))

        points.append(Coordinates([x_coord + 2*box_size/3, y_coord + box_size, z_coord]))
        points.append(Coordinates([x_coord + box_size/3, y_coord + box_size, z_coord]))
        points.append(Coordinates([x_coord + box_size/3, y_coord + box_size, z_coord + width]))
        points.append(Coordinates([x_coord + 2*box_size/3, y_coord + box_size, z_coord + width]))

        points.append(Coordinates([x_coord + box_size/3, y_coord + box_size, z_coord]))
        points.append(Coordinates([x_coord, y_coord + 2*box_size/3, z_coord]))
        points.append(Coordinates([x_coord, y_coord + 2*box_size/3, z_coord + width]))
        points.append(Coordinates([x_coord + box_size/3, y_coord + box_size, z_coord + width]))

        points.append(Coordinates([x_coord, y_coord + 2*box_size/3, z_coord]))
        points.append(Coordinates([x_coord, y_coord + box_size/3, z_coord]))
        points.append(Coordinates([x_coord, y_coord + box_size/3, z_coord + width]))
        points.append(Coordinates([x_coord, y_coord + 2*box_size/3, z_coord + width]))

        points.append(Coordinates([x_coord, y_coord + box_size/3, z_coord]))
        points.append(Coordinates([x_coord + box_size/3, y_coord, z_coord]))
        points.append(Coordinates([x_coord + box_size/3, y_coord, z_coord + width]))
        points.append(Coordinates([x_coord, y_coord + box_size/3, z_coord + width]))

        return points

    def get_faces(self, points, colour, c_coord):
        faces = []

        faces.append(Face(points[0:8], c_coord, colour, self))
        faces.append(Face(points[8:16], c_coord, colour, self))
        faces.append(Face(points[16:20], c_coord, colour, self))
        faces.append(Face(points[20:24], c_coord, colour, self))
        faces.append(Face(points[24:28], c_coord, colour, self))
        faces.append(Face(points[28:32], c_coord, colour, self))
        faces.append(Face(points[32:36], c_coord, colour, self))
        faces.append(Face(points[36:40], c_coord, colour, self))
        faces.append(Face(points[40:44], c_coord, colour, self))
        faces.append(Face(points[44:48], c_coord, colour, self))

        return faces

class Triangle():
    def __init__(self, x_coord, y_coord, z_coord, c_coord, colour, box_size,
        width = math.sqrt(2*(0.2/5)**2)):
        self.colour = colour
        self.subshapes = False
        self.line = False
        self.points = self.get_points(x_coord, y_coord, z_coord, box_size, width)
        self.faces = self.get_faces(self.points, colour, c_coord)
        self.name = "Triangle"

    def get_points(self, x_coord, y_coord, z_coord, box_size, width):
        points = []
        points.append(Coordinates([x_coord, y_coord, z_coord]))
        points.append(Coordinates([x_coord + box_size/2, y_coord + box_size, z_coord]))
        points.append(Coordinates([x_coord + box_size, y_coord, z_coord]))

        points.append(Coordinates([x_coord, y_coord, z_coord + width]))
        points.append(Coordinates([x_coord + box_size/2, y_coord + box_size, z_coord + width]))
        points.append(Coordinates([x_coord + box_size, y_coord, z_coord + width]))

        points.append(Coordinates([x_coord, y_coord, z_coord]))
        points.append(Coordinates([x_coord + box_size/2, y_coord + box_size, z_coord]))
        points.append(Coordinates([x_coord + box_size/2, y_coord + box_size, z_coord + width]))
        points.append(Coordinates([x_coord, y_coord, z_coord + width]))

        points.append(Coordinates([x_coord + box_size/2, y_coord + box_size, z_coord]))
        points.append(Coordinates([x_coord + box_size, y_coord, z_coord]))
        points.append(Coordinates([x_coord + box_size, y_coord, z_coord + width]))
        points.append(Coordinates([x_coord + box_size/2, y_coord + box_size, z_coord + width]))

        points.append(Coordinates([x_coord, y_coord, z_coord]))
        points.append(Coordinates([x_coord + box_size, y_coord, z_coord]))
        points.append(Coordinates([x_coord + box_size, y_coord, z_coord + width]))
        points.append(Coordinates([x_coord, y_coord, z_coord + width]))


        return points

    def get_faces(self, points, colour, c_coord):
        faces = []
        faces.append(Face([points[0], points[1], points[2]], c_coord, colour, self))
        faces.append(Face([points[3], points[4], points[5]], c_coord, colour, self))
        faces.append(Face([points[6], points[7], points[8], points[9]], c_coord, colour, self))
        faces.append(Face([points[10], points[11], points[12], points[13]], c_coord, colour, self))
        faces.append(Face([points[14], points[15], points[16], points[17]], c_coord, colour, self))

        return faces

class Empty_Box():
    def __init__(self, x_coord, y_coord, z_coord, c_coord, box_size, colour, square):
        self.colour = colour
        self.subshapes = False
        self.square = square
        self.line = False
        self.faces = self.get_faces(x_coord, y_coord, z_coord, c_coord, colour, box_size, square)
        self.c = self.square.c

        

    def get_faces(self, x, y, z, c_coord, colour, box_size, square):
        faces = [Face([Coordinates([x,y,z]), Coordinates([x+box_size,y,z]),
            Coordinates([x+box_size,y+box_size,z]),
            Coordinates([x,y+box_size,z])], c_coord, colour, self, square),
        Face([Coordinates([x,y+box_size,z+box_size]), Coordinates([x,y,z+box_size]),
            Coordinates([x+box_size,y,z+box_size]),
            Coordinates([x+box_size,y+box_size,z+box_size])], c_coord, colour, self, square),
        Face([Coordinates([x,y,z]), Coordinates([x,y,z+box_size]),
            Coordinates([x,y+box_size,z+box_size]),
            Coordinates([x,y+box_size,z])], c_coord, colour, self, square),
        Face([Coordinates([x,y,z,]), Coordinates([x,y,z+box_size]),
            Coordinates([x+box_size,y,z+box_size]), Coordinates([x+box_size,y,z])],
            c_coord, colour, self, square),
        Face([Coordinates([x,y+box_size,z]), Coordinates([x,y+box_size,z+box_size]),
            Coordinates([x+box_size,y+box_size,z+box_size]),
            Coordinates([x+box_size,y+box_size,z])], c_coord, colour, self, square),
        Face([Coordinates([x+box_size,y,z]), Coordinates([x+box_size,y,z+box_size]),
            Coordinates([x+box_size,y+box_size,z+box_size]),
            Coordinates([x+box_size,y+box_size,z])], c_coord, colour, self, square)]

        return faces


