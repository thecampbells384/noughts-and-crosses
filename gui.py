import tkinter
import math

import shapes
import AI


   
class Gui(tkinter.Tk):
    def __init__(self, menu_cube, menu_cube_display, game_space, threeD_game_display, fourD_game_display):
        tkinter.Tk.__init__(self)
        self._frame = None
        self.title("Noughts and Crosses")
        self.height = 800
        self.width = 1000
        self.geometry(f"{self.width}x{self.height}")
        self.configure(bg = "green")
        self.menu_cube = menu_cube
        self.menu_cube_display = menu_cube_display
        self.menu_graphic = self.menu_graphic()
        self.switch_frame(StartPage, None)
        self.User_choices = User_choices()
        self.game_space = game_space
        self.threeD_game_display = threeD_game_display
        self.fourD_game_display = fourD_game_display

    def menu_graphic(self):
        self.configure_menu_graphic(self.menu_cube, self.menu_cube_display)
        # self.menu_cube_display.frame = frame
        canvas = tkinter.Canvas(self, width=500, height=500,
            bg=self.menu_cube_display.background, highlightthickness=0) #
        
        canvas.pack()
        canvas.update()

        self.menu_cube_display.scale = 110
        self.menu_cube_display.display_shapes(self.menu_cube_display.shapes, 0, 0, 0, canvas)

        return canvas


    def switch_frame(self, frame_class, pack_instructions):
        new_frame = frame_class(self)
        if self._frame != None:
            self._frame.destroy()
        self._frame = new_frame
        if pack_instructions == "fill":
            self._frame.pack(fill="both")
        else:
            self._frame.pack()

    def valid_user_choices(self):
        match_counter = 0
        validity = True
        for colour in self.User_choices.colours:
            for comparison in self.User_choices.colours:
                if colour.get() == comparison.get():
                    match_counter +=1
        if match_counter > len(self.User_choices.colours):
            validity = False

        match_counter = 0
        for shape in self.User_choices.shapes:
            for comparison in self.User_choices.shapes:
                if shape.get() == comparison.get():
                    match_counter +=1
        if match_counter > len(self.User_choices.shapes):
            validity = False


        return validity

    def create_line_list(self, game_space):
        if self.User_choices.dimensions == 3:
            number_of_cubes = 1
        elif self.User_choices.dimensions == 4:
            number_of_cubes = 8

        line_reject_count = 0
        line_reject_list = []
        line_list = []
        line_name_list = []

        for cube in game_space.cubes[0:number_of_cubes]:
            for layer in cube.layers:
                for line in layer.lines:
                    if line.name in line_name_list:
                        line_reject_count += 1
                        line_reject_list.append(line.name)
                    elif line.alternate_name in line_name_list:
                        line_reject_count += 1
                        line_reject_list.append(line.alternate_name)
                    else:
                        line_list.append(line)
                        line_name_list.append(line.name)
                        line_name_list.append(line.alternate_name)

        print(len(line_list))
        return line_list



    def play_game(self, game_display, frame):
        if self.valid_user_choices():
            pack_instructions = 'fill'
            self.switch_frame(frame, pack_instructions)
            if self.User_choices.dimensions == 3:
                number_of_cubes = 1
            elif self.User_choices.dimensions == 4:
                number_of_cubes = 3

            line_list = self.create_line_list(self.game_space)
            ai = AI.AI(self.game_space, game_display, number_of_cubes, line_list)

            for cube in self.game_space.cubes[0:3]:
                for layer in cube.layers[0:3]:
                    for column in layer.columns:
                        for square in column.squares:
                            for face in square.content.faces:
                                face.game_space = self.game_space
                                face.window = game_display
                                face.gui = self
                                face.AI = ai

            for canvas in game_display.canvases:
                canvas.update()
            self.game_space.create_players(self.User_choices, self.game_space, game_display)
            for player in self.game_space.players:
                print(player.computer.get())
            
            if self.game_space.players[0].computer.get() == "computer":
                ai.computer_move()
            game_display.obtain_shapes(game_display, self.game_space)
            game_display.display_shapes(game_display.shapes)


            

        else:
            user_prompt = tkinter.Toplevel()
            message = tkinter.Message(user_prompt, text="Each player must have a unique colour and shape", font=(18))
            message.pack(padx=20, pady=20)
            button = tkinter.Button(user_prompt, text="Ok", command=lambda:user_prompt.destroy())
            button.pack()



        
        

    def configure_menu_graphic(self, menu_cube, menu_cube_display):
        menu_cube.cubes[0].layers[0].columns[0].squares[0].content = shapes.O(
            menu_cube.cubes[0].layers[0].columns[0].squares[0].x,
            menu_cube.cubes[0].layers[0].columns[0].squares[0].y,
            menu_cube.cubes[0].layers[0].columns[0].squares[0].z,
            menu_cube.cubes[0].layers[0].columns[0].squares[0].c,
            "yellow",
            menu_cube.cubes[0].layers[0].columns[0].squares[0].box_size)

        menu_cube.cubes[0].layers[1].columns[1].squares[1].content = shapes.O(
            menu_cube.cubes[0].layers[1].columns[1].squares[1].x,
            menu_cube.cubes[0].layers[1].columns[1].squares[1].y,
            menu_cube.cubes[0].layers[1].columns[1].squares[1].z,
            menu_cube.cubes[0].layers[1].columns[1].squares[1].c,
            "yellow",
            menu_cube.cubes[0].layers[1].columns[1].squares[1].box_size)

        menu_cube.cubes[0].layers[2].columns[0].squares[2].content = shapes.X(
            menu_cube.cubes[0].layers[2].columns[0].squares[2].x,
            menu_cube.cubes[0].layers[2].columns[0].squares[2].y,
            menu_cube.cubes[0].layers[2].columns[0].squares[2].z,
            menu_cube.cubes[0].layers[2].columns[0].squares[2].c,
            "blue",
            menu_cube.cubes[0].layers[2].columns[0].squares[2].box_size)

        menu_cube.cubes[0].layers[2].columns[1].squares[1].content = shapes.X(
            menu_cube.cubes[0].layers[2].columns[1].squares[1].x,
            menu_cube.cubes[0].layers[2].columns[1].squares[1].y,
            menu_cube.cubes[0].layers[2].columns[1].squares[1].z,
            menu_cube.cubes[0].layers[2].columns[1].squares[1].c,
            "blue",
            menu_cube.cubes[0].layers[2].columns[1].squares[1].box_size)

        menu_cube.cubes[0].layers[0].columns[2].squares[0].content = shapes.Triangle(
            menu_cube.cubes[0].layers[0].columns[2].squares[0].x,
            menu_cube.cubes[0].layers[0].columns[2].squares[0].y,
            menu_cube.cubes[0].layers[0].columns[2].squares[0].z,
            menu_cube.cubes[0].layers[0].columns[2].squares[0].c,
            "purple",
            menu_cube.cubes[0].layers[0].columns[2].squares[0].box_size)

        menu_cube.cubes[0].layers[0].columns[1].squares[0].content = shapes.Triangle(
            menu_cube.cubes[0].layers[0].columns[1].squares[0].x,
            menu_cube.cubes[0].layers[0].columns[1].squares[0].y,
            menu_cube.cubes[0].layers[0].columns[1].squares[0].z,
            menu_cube.cubes[0].layers[0].columns[1].squares[0].c,
            "purple",
            menu_cube.cubes[0].layers[0].columns[1].squares[0].box_size)


        menu_cube_display.obtain_shapes(menu_cube_display, menu_cube)

    def create_player_frames_and_labels(self, frame, player):
        player_frame = tkinter.Frame(frame, height=50, width= self.width,
            bg="yellow", borderwidth = 5, relief="groove")
        player_frame.pack(fill="both")
        player_label = tkinter.Label(player_frame, text=f"player {player+1}",
            font=18, bg="yellow")
        player_label.pack(side="left", padx=50, pady=50)

        return player_frame, player_label

    def create_colour_menu(self, frame, player, colour_options):
        colour = tkinter.StringVar(self)
        self.User_choices.colours.append(colour)
        self.User_choices.colours[player].set(colour_options[player])
        colour_menu = tkinter.OptionMenu(frame, self.User_choices.colours[player],
            *colour_options)
        colour_menu.pack(side="left", padx=20)

        return colour_menu

    def create_computer_menu(self, frame, player):
        computer = tkinter.StringVar(self)
        computer_options = ["human", "computer"]
        self.User_choices.computer.append(computer)
        self.User_choices.computer[player].set(computer_options[0])
        computer_menu = tkinter.OptionMenu(frame, self.User_choices.computer[player],
            *computer_options)
        computer_menu.pack(side="left", padx=20)

        return computer_menu

    def create_radio_buttons(self, frame, player, Shapes, canvas_height, canvas_width):
        radio_buttons= []
        canvases = []
        shape_choice = tkinter.IntVar(self)
        shape_choice.set(player)
        self.User_choices.shapes.append(shape_choice)
        shape_counter = 0
        for shape in Shapes:
            canvas = tkinter.Canvas(frame, height=canvas_height, width=canvas_width,
                bg="yellow")
            canvas.pack(side="left")
            if shape.subshapes:
                self.menu_cube_display.display_shapes(shape.shapes, 0, 0, 0, canvas)
            else:
                self.menu_cube_display.display_shapes(shape, 0, 0, 0, canvas)

            radio_button = tkinter.Radiobutton(frame,
                variable=self.User_choices.shapes[player], value=shape_counter,
                bg="yellow")
            radio_button.pack(side="left")
            shape_counter += 1
            canvases.append(canvas)
            radio_buttons.append(radio_button)

        return canvases, radio_buttons

class User_choices():
    def __init__(self):
        self.colours = []
        self.shapes = []
        self.dimensions = []
        self.computer = []


class StartPage(tkinter.Frame):
    def __init__(self, master):
        tkinter.Frame.__init__(self, master)
        master.User_choices = User_choices()
        master.menu_graphic.pack()


        
        tkinter.Button(self, text = "3D Noughts and Crosses", font=(18),
        command = lambda: master.switch_frame(threeD, None), width = 60, height = 2).pack()
        

        tkinter.Button(self, text = "4D Noughts and Crosses", font=(18),
        command = lambda: master.switch_frame(fourD, None), width = 60, height = 2).pack()
        

class threeD(tkinter.Frame):
    def __init__(self, master):
        tkinter.Frame.__init__(self, master)
        master.menu_graphic.pack_forget()
        master.User_choices.dimensions = 3
        box_size = 0.3
        Shapes = [
            shapes.O(box_size/2, box_size/2, 0, 0, "red", box_size, 0.03*2),
            shapes.X(box_size/2, box_size/2, 0, 0, "red", box_size, math.sqrt(2*(0.2/5)**2)*1.5, 0.3/5),
            shapes.Triangle(box_size/2, box_size/2, 0, 0, "red", box_size),
            shapes.Oct(box_size/2, box_size/2, 0, 0, "red", box_size)]
        player_frames = []
        player_labels = []
        colour_menus = []
        shape_canvases = []
        radio_buttons = []
        canvas_height = 50
        canvas_width = 70
        master.menu_cube_display.player_display_zeros = [canvas_height/2, canvas_width/2]
        colour_options = ["yellow", "blue", "orange", "purple", "pink", "brown", "black", "white"]

        for player in range(3):
            player_frame, player_label = master.create_player_frames_and_labels(self, player)
            player_frames.append(player_frame)
            player_labels.append(player_label)
            colour_menu = master.create_colour_menu(player_frames[player], player, colour_options)
            computer_menu = master.create_computer_menu(player_frames[player], player)
            colour_menus.append(colour_menu)
            canvases, radios = master.create_radio_buttons(
                player_frames[player], player, Shapes, canvas_height, canvas_width)
            radio_buttons.append(radios)
            shape_canvases.append(canvases)

        button_frame = tkinter.Frame(self, height=50, width= master.width, bg="green")
        button_frame.pack(fill="both")
        back_button = tkinter.Button(button_frame, text="Back", width = 30, height = 2, font = (18),
                  command=lambda: master.switch_frame(StartPage, None))
        play_button = tkinter.Button(button_frame, text="Play Game!", width = 30, height = 2, font = (18),
                  command=lambda: master.play_game(master.threeD_game_display, threeD_game))
        back_button.pack(side="left", padx=20, pady=20)
        play_button.pack(side="left")


class fourD(tkinter.Frame):
    def __init__(self, master):
        tkinter.Frame.__init__(self, master)
        master.menu_graphic.pack_forget()
        master.User_choices.dimensions = 4
        box_size = 0.3
        Shapes = [
            shapes.O(box_size/2, box_size/2, 0, 0, "red", box_size, 0.03*2),
            shapes.X(box_size/2, box_size/2, 0, 0, "red", box_size, math.sqrt(2*(0.2/5)**2)*1.5, 0.3/5),
            shapes.Triangle(box_size/2, box_size/2, 0, 0, "red", box_size),
            shapes.Oct(box_size/2, box_size/2, 0, 0, "red", box_size)]
        player_frames = []
        player_labels = []
        colour_menus = []
        shape_canvases = []
        radio_buttons = []
        canvas_height = 50
        canvas_width = 70
        master.menu_cube_display.player_display_zeros = [canvas_height/2, canvas_width/2]
        colour_options = ["yellow", "blue", "orange", "purple", "brown", "pink", "black", "white"]

        for player in range(4):
            player_frame, player_label = master.create_player_frames_and_labels(self, player)
            player_frames.append(player_frame)
            player_labels.append(player_label)
            colour_menu = master.create_colour_menu(player_frames[player], player, colour_options)
            computer_menu = master.create_computer_menu(player_frames[player], player)
            colour_menus.append(colour_menu)
            canvases, radios = master.create_radio_buttons(
                player_frames[player], player, Shapes, canvas_height, canvas_width)
            radio_buttons.append(radios)
            shape_canvases.append(canvases)
            
        button_frame = tkinter.Frame(self, height=50, width= master.width, bg="green")
        button_frame.pack(fill="both")
        back_button = tkinter.Button(button_frame, text="Back", width = 30, height = 2, font = (18),
                  command=lambda: master.switch_frame(StartPage, None))
        play_button = tkinter.Button(button_frame, text="Play Game!", width = 30, height = 2, font = (18),
                  command=lambda: master.play_game(master.fourD_game_display, fourD_game))
        back_button.pack(side="left", padx=20, pady=20)
        play_button.pack(side="left")

class threeD_game(tkinter.Frame):
    def __init__(self, master):
        tkinter.Frame.__init__(self, master)
        self.canvas_frame = tkinter.Frame(self, height=0.8*master.height, bg="green")
        self.canvases = []
        for canvas in range(1):
            canvas = tkinter.Canvas(self.canvas_frame, height=0.8*master.height, bg="green")
            self.canvases.append(canvas)
        self.player_display = tkinter.Frame(self, height=50, width= master.width - 200, bg="yellow")
        self.player_label = tkinter.Label(self.player_display, text = "cannon fodder", bg = "yellow")
        self.player_shape_display = tkinter.Canvas(self.player_display, height = 50, width = 200, bg="yellow")
        self.player_display.pack(fill = "both", side = "top")
        self.player_shape_display.pack(side="left")
        self.player_label.pack(side="left")
        self.canvas_frame.pack(expand=True, fill="x")
        for canvas in self.canvases:
            canvas.pack(side="left", expand=True, fill="both")
        self.button_frame = tkinter.Frame(self, bg="green")
        self.button_frame.pack()
        self.left_button = tkinter.Button(self.button_frame, text = "left", command= lambda: master.threeD_game_display.move_left())
        self.left_button.pack(side="left")
        self.right_button = tkinter.Button(self.button_frame, text = "right", command= lambda: master.threeD_game_display.move_right())
        self.right_button.pack(side="left")
        self.up_button = tkinter.Button(self.button_frame, text = "up", command= lambda: master.threeD_game_display.move_up())
        self.up_button.pack(side="left")
        self.down_button = tkinter.Button(self.button_frame, text = "down", command= lambda: master.threeD_game_display.move_down())
        self.down_button.pack(side="left")
        self.clockwise_button = tkinter.Button(self.button_frame, text = "clockwise", command= lambda: master.threeD_game_display.move_clockwise())
        self.clockwise_button.pack(side="left")
        self.anticlockwise_button = tkinter.Button(self.button_frame, text = "anti clockwise", command= lambda: master.threeD_game_display.move_anticlockwise())
        self.anticlockwise_button.pack(side="left")
        self.zoom_in_button = tkinter.Button(self.button_frame, text = "zoom in", command= lambda: master.threeD_game_display.zoom_in())
        self.zoom_in_button.pack(side="left")
        self.zoom_out_button = tkinter.Button(self.button_frame, text = "zoom out", command= lambda: master.threeD_game_display.zoom_out())
        self.zoom_out_button.pack(side="left")

        master.threeD_game_display.window = self
        master.threeD_game_display.canvas_frame = self.canvas_frame
        master.threeD_game_display.canvases = self.canvases
        master.threeD_game_display.player_display = self.player_display
        master.threeD_game_display.player_label = self.player_label
        master.threeD_game_display.player_shape_display = self.player_shape_display
        master.threeD_game_display.frame = self
        master.threeD_game_display.button_frame = self.button_frame

class fourD_game(tkinter.Frame):
    def __init__(self, master):
        tkinter.Frame.__init__(self, master)
        self.canvas_frame = tkinter.Frame(self, height=0.8*master.height, bg="green")
        self.canvases = []
        for canvas in range(3):
            canvas = tkinter.Canvas(self.canvas_frame, height=0.8*master.height, bg="green")
            self.canvases.append(canvas)
        self.player_display = tkinter.Frame(self, height=50, width= master.width - 200, bg="yellow")
        self.player_label = tkinter.Label(self.player_display, text = "cannon fodder", bg = "yellow")
        self.player_shape_display = tkinter.Canvas(self.player_display, height = 50, width = 200, bg="yellow")
        self.player_display.pack(fill = "both", side = "top")
        self.player_shape_display.pack(side="left")
        self.player_label.pack(side="left")
        self.canvas_frame.pack(expand=True, fill="x")
        for canvas in self.canvases:
            canvas.pack(side="left", expand=True, fill="both")
        self.button_frame = tkinter.Frame(self, bg="green")
        self.button_frame.pack()
        self.left_button = tkinter.Button(self.button_frame, text = "left", command= lambda: master.fourD_game_display.move_left())
        self.left_button.pack(side="left")
        self.right_button = tkinter.Button(self.button_frame, text = "right", command= lambda: master.fourD_game_display.move_right())
        self.right_button.pack(side="left")
        self.up_button = tkinter.Button(self.button_frame, text = "up", command= lambda: master.fourD_game_display.move_up())
        self.up_button.pack(side="left")
        self.down_button = tkinter.Button(self.button_frame, text = "down", command= lambda: master.fourD_game_display.move_down())
        self.down_button.pack(side="left")
        self.clockwise_button = tkinter.Button(self.button_frame, text = "clockwise", command= lambda: master.fourD_game_display.move_clockwise())
        self.clockwise_button.pack(side="left")
        self.anticlockwise_button = tkinter.Button(self.button_frame, text = "anti clockwise", command= lambda: master.fourD_game_display.move_anticlockwise())
        self.anticlockwise_button.pack(side="left")
        self.zoom_in_button = tkinter.Button(self.button_frame, text = "zoom in", command= lambda: master.fourD_game_display.zoom_in())
        self.zoom_in_button.pack(side="left")
        self.zoom_out_button = tkinter.Button(self.button_frame, text = "zoom out", command= lambda: master.fourD_game_display.zoom_out())
        self.zoom_out_button.pack(side="left")

        master.fourD_game_display.window = self
        master.fourD_game_display.canvas_frame = self.canvas_frame
        master.fourD_game_display.canvases = self.canvases
        master.fourD_game_display.player_display = self.player_display
        master.fourD_game_display.player_label = self.player_label
        master.fourD_game_display.player_shape_display = self.player_shape_display
        master.fourD_game_display.frame = self
        master.fourD_game_display.button_frame = self.button_frame

if __name__ == "__main__":
    window = []
    app = Gui()
    app.mainloop()