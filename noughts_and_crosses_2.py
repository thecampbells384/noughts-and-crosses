
import create_game_space
import game_display
import gui




game_space = create_game_space.create_board()
menu_cube = create_game_space.create_board()
menu_cube_display = game_display.Display_Game(500, 500, "noughts and crosses",
    "green", None, None, None, None, None, None, None)
threeD_game_display = game_display.Display_Game(1000, 700, "noughts and crosses", "green", None, None, None, None, None, None, None)
threeD_game_display.scale = 125
threeD_game_display.dimensions = 3
fourD_game_display = game_display.Display_Game(1000, 700, "noughts and crosses", "green", None, None, None, None, None, None, None)
fourD_game_display.scale = 100
fourD_game_display.dimensions= 4
menu_cube_display.dimensions = 3


menu = gui.Gui(menu_cube, menu_cube_display, game_space, threeD_game_display, fourD_game_display)


menu.mainloop()


