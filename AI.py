import time

import shapes

"""a class to control computer opponents"""

class AI:
    def __init__(self, game_space, game_display, number_of_cubes, line_list):
        """ The AI needs access to the game space (the position of all placed X's and O's)
        in order to calculate the best position for its next move. It needs access to the display
        so that each move can be viewed by the players. It needs to know if there is one cube or 3
        (3D or 4D). The line list is a list of all posible winning lines (49 in 3D, 222 in 4D). The
        lines are automatically mapped for each layer within the game_space module, and all duplicates
        are removed within the GUI module"""
        self.game_space = game_space
        self.game_display = game_display
        self.number_of_cubes = number_of_cubes
        self.line_list = line_list
        
    def insert_shape(self, player, square):
        """ A function to insert the appropriate shape for a given player to a given square"""
        if player.shape.name == "O":
            square.content = shapes.O(square.x, square.y,
                square.z, square.c, player.colour, square.box_size)
        elif player.shape.name == "X":
            square.content = shapes.X(square.x, square.y,
                square.z, square.c, player.colour, square.box_size)
        elif player.shape.name == "Triangle":
            square.content = shapes.Triangle(square.x, square.y,
                square.z, square.c, player.colour, square.box_size)
        elif player.shape.name == "Oct":
            square.content = shapes.Oct(square.x, square.y,
                square.z, square.c, player.colour, square.box_size)

    def next_player_list(self, player, game_space):
        """A function to produce a list of subsequent players in the order they will play.
        This is produced by using the switch_player function to cycle through all players,
        recording each player in turn, until getting back to the initial player"""
        player_list = []
        game_space.switch_player() # switch_pplayer turns the next player to play to active and deactivates the current player
        next_player = game_space.find_player() #find_player returns the player which is active
        while next_player != player:
            player_list.append(next_player)
            game_space.switch_player()
            next_player = game_space.find_player()



        return player_list


    def find_score(self, game_space, player):
        """A function to provide a score for a given situation described in the game space with
        respect to a given player. The function looks through all the lines in the line list
        and gives points for a winning line, and lines of one or two that remain unblocked by
        opponents. It deducts points for each opposition line of two that remains unblocked. The
        score from each line is tallied to evaluate the overall game position"""
        win_score = 100000000
        no_block_score_deduction = 100000
        two_in_row_score = 30000
        one_in_row_score = 3000
        score = 0
        next_player_list = self.next_player_list(player, game_space) # A list of all the opponents in the order they will play
        # line_count = 0


        for line in self.line_list:
            # line_count += 1
            if type(line.square1.content) == type(line.square2.content) == \
            type(line.square3.content) == type(player.shape): # if winning line add win_score to score
                score += win_score
            next_player_count = 1
            for next_player in next_player_list:
                """This loop checks if the line under examination contains two unblocked pieces for each
                of the subsequent player. A no block score deduction is applied if a player remains unblocked.
                The deduction is divided by the number of turns away from playing the player needing blocked is,
                as leaving the next player unblocked results in certain defeat, likewise leaving the following player
                with two options to win, or the third player with 3 options to win"""
                if type(line.square1.content) == type(line.square2.content) == \
                type(next_player.shape) and type(line.square3.content) == shapes.Empty_Box:
                    score -= no_block_score_deduction/next_player_count
                if type(line.square1.content) == type(line.square3.content) == \
                type(next_player.shape) and type(line.square2.content) == shapes.Empty_Box:
                    score -= no_block_score_deduction/next_player_count
                if type(line.square3.content) == type(line.square2.content) == \
                type(next_player.shape) and type(line.square1.content) == shapes.Empty_Box:
                    score -= no_block_score_deduction/next_player_count
                next_player_count += 1

            """The line under examination is awarded a two in a row score if it contains
            two of the player's shapes without being blocked"""
            if type(line.square1.content) == type(line.square2.content) == \
            type(player.shape) and type(line.square3.content) == shapes.Empty_Box:
                score += two_in_row_score
            if type(line.square1.content) == type(line.square3.content) == \
            type(player.shape) and type(line.square2.content) == shapes.Empty_Box:
                score += two_in_row_score
            if type(line.square3.content) == type(line.square2.content) == \
            type(player.shape) and type(line.square1.content) == shapes.Empty_Box:
                score += two_in_row_score

            """ The line under examination is awarded a one in a row score if it contains
            one of the player's shapes without being blocked"""
            if type(line.square1.content) == type(line.square2.content) == \
            shapes.Empty_Box and type(line.square3.content) == type(player.shape):
                score += one_in_row_score
            if type(line.square1.content) == type(line.square3.content) == \
            shapes.Empty_Box and type(line.square2.content) == type(player.shape):
                score += one_in_row_score
            if type(line.square3.content) == type(line.square2.content) == \
            shapes.Empty_Box and type(line.square1.content) == type(player.shape):
                score += one_in_row_score

        # print(line_count)
        return score


    def track_score(self, score_list, score, square_number):
        score_list[square_number] = score


    def find_best_move(self, player):
        best_square = []
        best_score = 0
        score_list = {}
        for cube in self.game_space.cubes[0:self.number_of_cubes]:
            for layer in cube.layers[0:3]:
                for column in layer.columns:
                    for square in column.squares:
                        if type(square.content) == shapes.Empty_Box:
                            game_display = square.content.faces[0].window
                            ai = square.content.faces[0].AI
                            gui = square.content.faces[0].gui
                            game_space = square.content.faces[0].game_space
                            if best_square == []:
                                best_square = square
                            self.insert_shape(player, square)
                            score = self.find_score(self.game_space, player)
                            self.track_score(score_list, score, square.number)
                            if score > best_score or (best_score == 0):
                                best_square = square
                                best_score = score
                            square.content = shapes.Empty_Box(square.x, square.y, square.z, square.c, square.box_size, "red", square)
                            for face in square.content.faces:
                                face.window = game_display
                                face.AI = ai
                                face.gui = gui
                                face.game_space = game_space
        # for cube in self.game_space.cubes:
        #     for layer in cube.layers[0:2]:
        #         for column in layer.columns:
        #             for square in column.squares:
        #                 if square.number == best_square_copy.number:
        #                     best_square = square

        print(score_list)
        print(f"best score: {best_score}")
        print(f"best_square: {best_square}")

        return best_square

    def computer_move(self):
        # time.sleep(2)
        player = self.game_space.find_player()
        best_square = self.find_best_move(player)
        face = best_square.content.faces[0]

        self.insert_shape(player, best_square)
      
        self.game_display.obtain_shapes(self.game_display, self.game_space)
        self.game_display.display_shapes(self.game_display.shapes)

        won = False

        for cube in self.game_space.cubes:
                win, start_point, end_point = cube.check_win()
                if win:
                    won = True
                    face.display_win(player)
                    break
                    
        if not won:
            self.game_space.switch_player()
            player = self.game_space.find_player()
            self.game_display.display_player(player, player.shape)
            if player.computer.get() == "computer":
                face.AI.computer_move()


