import tkinter
import math
import numpy
from operator import attrgetter
from statistics import mean


class Projected_Point:
    def __init__(self, points):
        self.points = points

class Centered_Points:
    def __init__(self, points):
        self.points = points




class Display_Game:
    def __init__(self, width, height, title, background,
        window = "std",
        frame = "std",
        canvas_number = 1,
        canvas = "std",
        player_display = "std",
        player_label = "std",
        player_shape_display = "std",
        shapes=[], x_angle=0, y_angle=0, z_angle=0, scale = 150):
        #calculate center of screen
        # self.zeros = [int(width/2), int(height/2)]
        # self.player_display_zeros = [int(200/2), int(50/2)]
        self.dimensions = []

        #initialize tkinter window for displaying graphics unless a window is already provided
        if window == "std":
            self.window = tkinter.Tk()
            self.window.title(title)
        else:
            self.window = window
        self.width = width
        self.height = height
        self.background = background
        if frame == "std":
            self.frame = tkinter.Frame(self.window, height=height, width= width, bg="green")
            self.canvas_frame = tkinter.Frame(self.frame, height=0.9*height, bg="green")
            self.canvases = []
            for canvas in range(canvas_number):
                canvas = tkinter.Canvas(self.canvas_frame, height=0.9*height, bg=background)
                self.canvases.append(canvas)
            self.player_display = tkinter.Frame(self.window, height=50, width= width - 200, bg="yellow")
            self.player_label = tkinter.Label(self.player_display, text = "cannon fodder", bg = "yellow")
            self.player_shape_display = tkinter.Canvas(self.player_display, height = 50, width = 200, bg="yellow")
        else:
            self.frame = frame
            self.canvases = []
            self.canvases.append(canvas)
            self.player_display = player_display
            self.player_label = player_label
            self.player_shape_display = player_shape_display
        if player_display != None:
            self.player_display.pack(fill = "both", side = "top")
        if player_shape_display != None:
            self.player_shape_display.pack(side = "left")
        if canvas != None:
            self.canvas_frame.pack(expand=True, fill="x")
            for canvas in self.canvases:
                canvas.pack(side="left", expand=True, fill="both")
        if frame != None:
            self.frame.pack(expand=True, fill="both")
            self.button_frame = tkinter.Frame(self.frame, bg="green")
            self.button_frame.pack()
            self.left_button = tkinter.Button(self.button_frame, text = "left", command= lambda: self.move_left())
            self.left_button.pack(side="left")
            self.right_button = tkinter.Button(self.button_frame, text = "right", command= lambda: self.move_right())
            self.right_button.pack(side="left")
            self.up_button = tkinter.Button(self.button_frame, text = "up", command= lambda: self.move_up())
            self.up_button.pack(side="left")
            self.down_button = tkinter.Button(self.button_frame, text = "down", command= lambda: self.move_down())
            self.down_button.pack(side="left")
            self.clockwise_button = tkinter.Button(self.button_frame, text = "clockwise", command= lambda: self.move_clockwise())
            self.clockwise_button.pack(side="left")
            self.anticlockwise_button = tkinter.Button(self.button_frame, text = "anti clockwise", command= lambda: self.move_anticlockwise())
            self.anticlockwise_button.pack(side="left")
            self.zoom_in_button = tkinter.Button(self.button_frame, text = "zoom in", command= lambda: self.zoom_in())
            self.zoom_in_button.pack(side="left")
            self.zoom_out_button = tkinter.Button(self.button_frame, text = "zoom out", command= lambda: self.zoom_out())
            self.zoom_out_button.pack(side="left")
        self.x_angle = x_angle
        self.y_angle = y_angle
        self.z_angle = z_angle
        self.scale = scale
        self.shapes = shapes
        self.board = []
        self.cumulative_x_rotate = 0
        self.cumulative_y_rotate = 0
        self.cumulative_z_rotate = 0


    def display_player(self, player, shape):
        """Displays the current player name and shape in the player_display at
        the top of the game screen"""
        self.player_label.destroy() #clears previous player's name
        # Displays new player's name
        self.player_label = tkinter.Label(self.player_display,
            text = player.name, font = (None, 15), bg = "yellow")
        self.player_label.pack(side="left")
        # sends new player's shape to the player_shape_display canvas
        if shape.subshapes: #if shape is constructed out of multiple smaller shapes
           self.display_shapes(shape.shapes,0, 0, 0, self.player_shape_display)
        else:
            self.display_shapes(shape,0, 0, 0, self.player_shape_display)



    def zoom_out(self):
        self.scale = self.scale - 25
        self.clear()
        self.display_shapes(self.shapes, self.x_angle, self.y_angle, self.z_angle)

    def zoom_in(self):
        self.scale = self.scale + 25
        self.clear()
        self.display_shapes(self.shapes, self.x_angle, self.y_angle, self.z_angle)

    def move_left(self):
        self.x_angle = self.x_angle + 0.4
        self.clear()
        self.display_shapes(self.shapes, self.x_angle, self.y_angle, self.z_angle)

    def move_right(self):
        self.x_angle = self.x_angle - 0.4
        self.clear()
        self.display_shapes(self.shapes, self.x_angle, self.y_angle, self.z_angle)

    def move_up(self):
        self.y_angle = self.y_angle + 0.4
        self.clear()
        self.display_shapes(self.shapes, self.x_angle, self.y_angle, self.z_angle)

    def move_down(self):
        self.y_angle = self.y_angle - 0.4
        self.clear()
        self.display_shapes(self.shapes, self.x_angle, self.y_angle, self.z_angle)

    def move_clockwise(self):
        self.z_angle = self.z_angle + 0.4
        self.clear()
        self.display_shapes(self.shapes, self.x_angle, self.y_angle, self.z_angle)

    def move_anticlockwise(self):
        self.z_angle = self.z_angle - 0.4
        self.clear()
        self.display_shapes(self.shapes, self.x_angle, self.y_angle, self.z_angle)

    def rotate_shape(self, shape, x_angle, y_angle, z_angle):
        """rotates a single shape along its x, y, and z axes by given angles"""
        # A list used to find and store the average z coordinate for a face
        list_x_points = []
        list_y_points = []
        list_z_points = []

        # A list used to find and store the average z coordinate for a shape
        list_x_face = []
        list_y_face = []
        list_z_face = []

        #performs the rotation
        for face in shape.faces:          
            for point in face.points:
                point.rotate("x", x_angle, self.scale)
                point.rotate("y", y_angle, self.scale)
                point.rotate("z", z_angle, self.scale)
                list_z_points.append(point.z)
                list_y_points.append(point.y)
                list_x_points.append(point.x)
            face.z = mean(list_z_points)
            face.y = mean(list_y_points)
            face.x = mean(list_x_points)
            list_x_points = []
            list_y_points = []
            list_z_points = []
            list_z_face.append(face.z)
            list_y_face.append(face.y)
            list_x_face.append(face.x)
        #updates the x,y,z coordinates for the centre of the shape
        shape.z = mean(list_z_face)
        shape.y = mean(list_y_face)
        shape.x = mean(list_x_face)

    def rotate_line(self, line, x_angle, y_angle, z_angle):
        """rotates a line along x, y, and z axes by given angles"""
        line.start_point.rotate("x", x_angle, self.scale)
        line.start_point.rotate("y", y_angle, self.scale)
        line.start_point.rotate("z", z_angle, self.scale)
        line.end_point.rotate("x", x_angle, self.scale)
        line.end_point.rotate("y", y_angle, self.scale)
        line.end_point.rotate("z", z_angle, self.scale)
        #defines the coordinates of a line as its mid-point
        line.x = (line.end_point.x - line.start_point.x)/2
        line.y = (line.end_point.y - line.start_point.y)/2
        line.z = (line.end_point.z - line.start_point.z)/2

    def rotate_points(self, shapes, x_angle=0, y_angle=0, z_angle=0):
        """Takes a list of shapes and adjusts their x, y, and z coordinates
        based on given rotational angles."""
              
        if type(shapes) == list:
            for shape in shapes:
                if shape.line:
                    self.rotate_line(shape, x_angle, y_angle, z_angle)
                else:
                    self.rotate_shape(shape, x_angle, y_angle, z_angle)
        else:
            self.rotate_shape(shapes, x_angle, y_angle, z_angle)


    def sort_faces(self, shapes):
        """Takes a list of shapes and sorts them by distance from the screen.
        Also sorts the faces within the shape by the same criteria. Updates each
        face's distance from the viewer and orientation vector"""
        if type(shapes) == list: # if more than one shape is to be sorted
            shapes.sort(key = attrgetter("z"), reverse = True)        
            for shape in shapes:
                if shape.line == False:
                    for face in shape.faces:
                        face.distance = math.sqrt((face.z + 5)**2 + face.x**2 + face.y**2)
                        # Vector from the centre of the shape to the centre of the face
                        # Determines which direction the face is facing
                        face.orientation_vector = numpy.array([(face.x - shape.x), (face.y - shape.y), (face.z - shape.z)])

            for shape in shapes:
                if shape.line == False:
                    shape.faces.sort(key = attrgetter("distance"), reverse = True)
            return shapes
        else:
            if shapes.line == False:
                    for face in shapes.faces:
                        face.distance = math.sqrt((face.z + 5)**2 + face.x**2 + face.y**2)
                        # Vector from the centre of the shape to the centre of the face
                        # Determines which direction the face is facing
                        face.orientation_vector = numpy.array([(face.x - shapes.x), (face.y - shapes.y), (face.z - shapes.z)])
                    return shapes

    def display_shapes(self, shapes, x_angle=0, y_angle=0, z_angle=0, canvas="self.image"):
        """Takes a list of shapes, rotates them by the given angles, sorts them 
        by distance from the screen, converts the coordinates into a 2D 
        representation, and displays them on the Game_Display"""

        #clears the appropriate canvas so that the image can be updated with new shapes
        #if the canvas provided does not live within Display_Game then it is assumed
        #to be clear already
        if canvas == "self.image":
            self.clear()
        elif canvas == self.player_shape_display:
            self.player_shape_display.delete("all")
        
        self.rotate_points(shapes, x_angle, y_angle, z_angle)
        # cumulative rotation angles are stored so that new shapes may be added
        # in the correct orientation
        self.cumulative_x_rotate += self.x_angle
        self.cumulative_y_rotate += self.y_angle
        self.cumulative_y_rotate += self.z_angle
        #angles are reset after rotation ready for next user input
        self.x_angle = 0
        self.y_angle = 0
        self.z_angle = 0
        
        shapes = self.sort_faces(shapes)
        self.project_points(shapes, canvas)

    def face_visible(self, face, view_point = [0, 0, -5]):
        """A function that calculates the dot product of the vector from the view
        point to the centre of the face, with the orientation vector of the face.
        If the result is negative then the face should be visible and True is 
        returned."""
        view_of_face = numpy.array([face.x - view_point[0], face.y - view_point[1], face.z - view_point[2]])
        if numpy.dot(view_of_face, face.orientation_vector) < 0:
            return True
        else:
            return False


    def project_points(self, shapes, frame="self.image"):
        """Takes a list of shapes, built from faces described by 3D coordinates.
         The function produces 2D coordinates to represent the 3D shape.
         Finally the shapes are displayed using the createShape function"""

        if type(shapes) == list: # this means there is more than one shape to display
            for shape in shapes:
                if shape.line == False: #shape is not a line and so has faces
                    for face in shape.faces:
                        face.Projected_Points = []
                        #finds 2D coordinates to represent a 3D point
                        for point in face.points:
                            face.Projected_Points.append(point.Projected_Point)

                        if self.face_visible(face): #displays visible faces
                            self.createShape(face, frame)
                else:
                    self.create_line(shape) #draws a line

        else: #there is only a single shape to display
            if shapes.line == False: #shape is not a line and so has faces
                for face in shapes.faces:
                    face.Projected_Points = []
                    #finds 2D coordinates to represent a 3D point
                    for point in face.points:
                        face.Projected_Points.append(point.Projected_Point)

                    if self.face_visible(face): #displays visible faces
                        self.createShape(face, frame)
            else:
                self.create_line(shape) #draws a line        

    def create_line(self, line):
        """This function draws a line with a given start and end point"""
        # The coordinate points are adjusted so that the orign is the centre of the canvas
        centered_start_point = []
        centered_end_point = []
        #start point x coordinate
        centered_start_point.append(line.start_point.Projected_Point.points[0] + self.zeros[0])
        #start point y coordinate
        centered_start_point.append(line.start_point.Projected_Point.points[1] + self.zeros[1])

        #end point x coordinate
        centered_end_point.append(line.end_point.Projected_Point.points[0] + self.zeros[0])
        #end point y coordinate
        centered_end_point.append(line.end_point.Projected_Point.points[1] + self.zeros[1])

        #the line is drawn to the Canvas
        self.image.create_line(centered_start_point[0], centered_start_point[1],
            centered_end_point[0], centered_end_point[1], fill = line.colour, width = 5)

    def createShape(self, face, canvas="self.image", active_fill="yellow"):
        #calculate centered coorinates for shape
        centered_points = []
        if canvas == "self.image":
            # self.canvases[face.c].update()
            for projected_point in face.Projected_Points:
                centered_points.append(projected_point.points[0] + self.canvases[face.c].winfo_width()/2)
                centered_points.append(projected_point.points[1] + self.canvases[face.c].winfo_height()/2)
            face.Centered_Points = Centered_Points(centered_points)
            #draw shape on screen
        
            face.physical_face = self.canvases[face.c].create_polygon(face.Centered_Points.points, fill=face.colour, outline="black", activefill=active_fill)
            self.canvases[face.c].tag_bind(face.physical_face, "<ButtonPress-1>", face.clicked)
        else:
            # canvas.update()
            for projected_point in face.Projected_Points:
                centered_points.append(projected_point.points[0] + canvas.winfo_width()/2)
                centered_points.append(projected_point.points[1] + canvas.winfo_height()/2)
            face.Centered_Points = Centered_Points(centered_points)
            canvas.create_polygon(face.Centered_Points.points, fill=face.colour, outline="black")
            # print(f"centered_points: {face.game_space.cubes[0].layers[1].columns[1].squares[1].content.faces[0].Centered_Points}")

    def clear(self):
        #clear display
        if type(self.canvases) == list:
            for canvas in self.canvases:
                canvas.delete("all")
        else:
            self.canvases.delete("all")

    def mainloop(self):
        self.window.mainloop()

    def obtain_shapes(self, window, game_space):
        """takes the shapes stored within the game_space and sends them to
        Display_Game.shapes to be printed to the canvas"""
        window.shapes = []
        if window.dimensions == 3:
            for layer in game_space.cubes[0].layers[0:3]:
                for column in layer.columns:
                    for square in column.squares:
                        if square.content.subshapes:
                            for shape in square.content.shapes:
                                window.shapes.append(shape)
                        else:
                            window.shapes.append(square.content)

            for shape in game_space.cubes[0].board.shapes:
                window.shapes.append(shape)
        elif window.dimensions == 4:             
            for cube in game_space.cubes[0:3]:
                for layer in cube.layers[0:3]:
                    for column in layer.columns:
                        for square in column.squares:
                            if square.content.subshapes:
                                for shape in square.content.shapes:
                                    window.shapes.append(shape)
                            else:
                                window.shapes.append(square.content)
            for cube in game_space.cubes[0:3]:
                for shape in cube.board.shapes:
                    window.shapes.append(shape)


class Coordinates:
    def __init__(self, point, scale = 150):
        """Takes an x, y, z coordinate. Provides functions for rotating a point
        around x, y, and z axes, as well as obtaining 2D coordinates to represent
        3D geometry""" 
        (x,y,z) = point
        self.x = x
        self.y = y
        self.z = z
        self.Projected_Point = Projected_Point(self.flatten(scale,5))

    def flatten(self, scale, distance):
        #calculate 2D coordinates from 3D point
        projectedY = int(((self.y * distance) / (self.z + distance)) * scale)
        projectedX = int(((self.x * distance) / (self.z + distance)) * scale)
        return (projectedX, projectedY)

    def rotate(self, axis, angle, scale = 150):
        #rotate point around axis
        angle = angle / 450 * 180 / math.pi
        sqrt2 = math.sqrt(2)
        if axis == 'z':
            #rotate aroud Z axis
            newX = self.x * math.cos(angle) - self.y * math.sin(angle)
            newY = self.y * math.cos(angle) + self.x * math.sin(angle)
            newZ = self.z
        elif axis == 'x':
            #rotate around X axis
            newY = self.y * math.cos(angle) - self.z * math.sin(angle)
            newZ = self.z * math.cos(angle) + self.y * math.sin(angle)
            newX = self.x
        elif axis == 'y':
            #rotate around Y axis
            newX = self.x * math.cos(angle) - self.z * math.sin(angle)
            newZ = self.z * math.cos(angle) + self.x * math.sin(angle)
            newY = self.y
        else:
            raise ValueError('not a valid axis')
        self.x = newX
        self.y = newY
        self.z = newZ
        self.Projected_Point = Projected_Point(self.flatten(scale,5))



# if __name__ == "__main__":

                



